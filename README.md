# Python / PyPy version of the Alaric final-state parton shower


## Getting started

Install [PyPy](https://www.pypy.org/) and [mpi4py](https://pypi.org/project/mpi4py/)

Clone the Alaric repository:

```
git clone https://gitlab.com/shoeche/pyalaric.git
```

## Run the code

This repository implements the tests of the final-state parton-shower evolution algorithm discussed in [arXiv:2208:06057](https://arxiv.org/abs/2208.06057).

A typical command line invocation to produce the distributions shown in Figs. 3 and 4 would be
```
mpirun -n 16 pypy3 alaric.py -e40000 -L -F[-0.7,0] -O-1 -A0.005 -b0 -C0.01 -n1024
```
